# UltimateKEYS Keyboard Layout - Core for Linux (XKB)

**International US QWERTY layout with AltGr (Right Alt) dead keys (Compose Key).**  
This layout is based on US QWERTY, and has borrowed some aspects from EurKEY and US-International.

This release (and repository) only contains the main keyboard layout of UltimateKEYS (in the XKB format).

The additional customized Compose Key sequences (Multi_key) have not been included in this release/repository, since these may change more frequently over time.

As such, I only consider the main layout of UltimateKEYS as the 'core element'.

Also note that the files 'us_code-snippet.txt' and 'custom' have the same functionality ('custom' provided for convenience during testing).

To try this layout, put the file 'custom' into "/usr/share/X11/xkb/symbols/", then activate it by selecting "A user-defined custom Layout".

See the folder `xkb-symbols`.

## Keyboard Layout Image

![UltimateKEYS - Keyboard Layout Image](images/UltimateKEYS_Keyboard_Layout_Image.png)

=&gt; CP key (blue) : &nbsp;Compose Key (Multi_key), in combination with AltGr (Right Alt).  
=&gt; Note that the ISO key (next to Left Shift) is not present on standard ANSI keyboards.

## Websites and Documentation

**Websites&nbsp;: &nbsp;[www.ultimatekeys.info](https://pieter-degroote.github.io/UltimateKEYS/) ~ [www.pieter-degroote.info](https://pieter-degroote.github.io/)**

**GitHub repository&nbsp;: &nbsp;https://github.com/pieter-degroote/UltimateKEYS**
