# UltimateKEYS Keyboard Layout - Core for Linux (XKB)

**International US QWERTY layout with AltGr (Right Alt) dead keys (Compose Key).**  
This layout is based on US QWERTY, and has borrowed some aspects from EurKEY and US-International.

This release (and repository) only contains the main keyboard layout of UltimateKEYS (in the XKB format).

The additional customized Compose Key sequences (Multi_key) have not been included in this release/repository, since these may change more frequently over time.

As such, I only consider the main layout of UltimateKEYS as the 'core element'.

Also note that the files 'us_code-snippet.txt' and 'custom' have the same functionality ('custom' provided for convenience during testing).

To try this layout, put the file 'custom' into "/usr/share/X11/xkb/symbols/", then activate it by selecting "A user-defined custom Layout".

## Full Integration into XKB (for developers)

1. Append the contents of 'us_code-snippet.txt' to the file "/usr/share/X11/xkb/symbols/us" (+ make backup copy).

2. Open the file "/usr/share/X11/xkb/rules/evdev.xml" (+ make backup copy). Add a new variant to the list "English (US)", under `<variantList>`&nbsp;:

       <variant>
         <configItem>
           <name>ultkeys</name>
           <description>English (US, UltimateKEYS)</description>
         </configItem>
       </variant>

3. Open the file "/usr/share/X11/xkb/rules/evdev.lst" (+ make backup copy). Add the following line to the `! variant` section&nbsp;:

       ultkeys         us: English (US, UltimateKEYS)

4. Restart, the new layout should be available as "English (US, UltimateKEYS)", under "English (US)".

## Keyboard Layout Image

![UltimateKEYS - Keyboard Layout Image](/images/UltimateKEYS_Keyboard_Layout_Image.png)

=&gt; CP key (blue) : &nbsp;Compose Key (Multi_key), in combination with AltGr (Right Alt).  
=&gt; Note that the ISO key (next to Left Shift) is not present on standard ANSI keyboards.
